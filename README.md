# NEOVIM CONFIG OF 'SUM4N

## Dashboard

![](glance/nv-dash.png)

## File-Explorer

![](glance/nv-tree.png)

## Telescope FuzzyFinder

![](glance/nv-telescope.png)

## Terminal

![](glance/nv-term.png)

## Lsp-Builtin

![](glance/nv-lsp.png)

This is my NeoVim configuration. Written in Lua.

## Plugins Are:
* Nvim Tree
* Tree Sitter
* FuzzyFinder
* **EverForest | TokyoNight | Dracula | GruvBox | Gotham**
* Terminal
* Lsp Built-in
* CmpBuffer
**And More...** 

Thank you
