local ensure_packer = function()
	local fn = vim.fn
	local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
	if fn.empty(fn.glob(install_path)) > 0 then
		fn.system({ "git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path })
		vim.cmd([[packadd packer.nvim]])
		return true
	end
	return false
end

local packer_bootstrap = ensure_packer()

-- Autocommand that reloads neovim whenever you save this file
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plug-setup.lua source <afile> | PackerSync
  augroup end
]])

-- load packer
local status, packer = pcall(require, "packer")
if not status then
	return
end

-- Have packer use a popup window
packer.init({
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "rounded" })
		end,
	},
})

-- installing my preffered plugins

return packer.startup(function(use)
	use("wbthomason/packer.nvim")

	use("nvim-lua/plenary.nvim") -- lua functions that many plugins use

	use("christoomey/vim-tmux-navigator") -- tmux & split window navigation

	use("onsails/lspkind.nvim") -- vs-code like icons for autocompletion

	-- file manager
	-- use 'preservim/nerdtree'

	-- Dashboard is a nice start screen for nvim
	use({ "glepnir/dashboard-nvim" })

	-- file explorer
	use("nvim-tree/nvim-tree.lua")
	use("stevearc/oil.nvim")

	-- statusline
	use("nvim-lualine/lualine.nvim")

	-- my language server and autocompletion
	-- use {'neoclide/coc.nvim', branch = 'release'}

	-- for cool icons
	use("kyazdani42/nvim-web-devicons")

	-- fuzzy finding telescope
	use({ "nvim-telescope/telescope-fzf-native.nvim", run = "make" }) -- dependency for better sorting performance
	use({ "nvim-telescope/telescope.nvim", branch = "0.1.x" }) -- fuzzy finder

	-- colorizer plug
	use("norcalli/nvim-colorizer.lua")

	-- commenting with gc
	use("numToStr/Comment.nvim")

	-- autocompletion
	use("hrsh7th/nvim-cmp") -- completion plugin
	use("hrsh7th/cmp-buffer") -- source for text in buffer
	use("hrsh7th/cmp-path") -- source for file system path

	-- snippets
	use("L3MON4D3/LuaSnip") -- snippet engine
	use("saadparwaiz1/cmp_luasnip") -- for autocompletion
	use("rafamadriz/friendly-snippets") -- useful snippets

	-- autocomplete tags & pairs
	use({ "windwp/nvim-ts-autotag", after = "nvim-treesitter" }) -- autoclose tags
	use("windwp/nvim-autopairs")
	-- for surround
	use("tpope/vim-surround")
	use("tpope/vim-repeat")

	-- treesitter configuration
	use({
		"nvim-treesitter/nvim-treesitter",
		run = function()
			local ts_update = require("nvim-treesitter.install").update({ with_sync = true })
			ts_update()
		end,
	})

	-- blank-line indention
	use("lukas-reineke/indent-blankline.nvim")

	-- Terminal
	use("akinsho/toggleterm.nvim")

	-- git integration
	use("lewis6991/gitsigns.nvim") -- show line modifications on left hand side

	-- Colorschemes
	use("folke/tokyonight.nvim")
	use("Mofiqul/dracula.nvim")
	-- use 'ellisonleao/gruvbox.nvim'
	use("joshdick/onedark.vim")
	use("sainnhe/everforest")
	use("whatyouhide/vim-gotham")
	use("sainnhe/gruvbox-material")
	use("NTBBloodbath/doom-one.nvim")
	use("EdenEast/nightfox.nvim")

	-- which-key
	use("folke/which-key.nvim")

	-- managing & installing lsp servers
	use("williamboman/mason.nvim") -- in charge of managing lsp servers, linters & formatters
	use("williamboman/mason-lspconfig.nvim") -- bridges gap b/w mason & lspconfig
	-- linters & formatters
	use("jose-elias-alvarez/null-ls.nvim")
	use("jayp0521/mason-null-ls.nvim")

	-- configuring lsp servers
	use("neovim/nvim-lspconfig") -- easily configure language servers
	use("hrsh7th/cmp-nvim-lsp") -- for autocompletion
	use({ "glepnir/lspsaga.nvim", commit = "b7b4777", branch = "main" }) -- enhanced lsp uis

	-- for neovim startup performance
	-- use 'lewis6991/impatient.nvim'

	if packer_bootstrap then
		require("packer").sync()
	end
end)
