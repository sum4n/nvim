local setup, blankline = pcall(require, "ibl")
if not setup then
	return
end

vim.opt.list = true

blankline.setup({
	indent = {
		char = "┊",
	},
	whitespace = {
		highlight = "IblWhitespace",
		remove_blankline_trail = true,
	},
	scope = {
		enabled = true,
		char = nil,
		show_start = true,
		show_end = true,
		injected_languages = true,
		highlight = "IblScope",
		priority = 1024,
		include = {
			node_type = {},
		},
		-- exclude = {
		-- 	language = {},
		-- 	node_type = {
		-- 		["*"] = {
		-- 			"source_file",
		-- 			"program",
		-- 		},
		-- 		lua = {
		-- 			"chunk",
		-- 		},
		-- 		python = {
		-- 			"module",
		-- 		},
		-- 	},
		-- },
	},
	show_current_context = true,
	show_current_context_start = true,
	exclude = {
		filetypes = { "dashboard" },
	},
})
