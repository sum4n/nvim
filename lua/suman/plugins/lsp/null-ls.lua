-- import null-ls plugin safely
local setup, null_ls = pcall(require, "null-ls")
if not setup then
	return
end

-- for conciseness
local formatting = null_ls.builtins.formatting -- to setup formatters
local diagnostics = null_ls.builtins.diagnostics -- to setup linters
local code_actions = null_ls.builtins.code_actions -- to do available actions
local completion = null_ls.builtins.completion -- for completions
local hover = null_ls.builtins.hover -- for hover

-- to setup format on save
local augroup = vim.api.nvim_create_augroup("LspFormatting", {})

-- configure null_ls
null_ls.setup({
	-- setup formatters & linters
	sources = {

		--[ formatting ]--
		-- "formatting.prettier.with({disabled_filetypes: {}})"  -- to disable file types
		-- formatting.prettier, -- js/ts
		formatting.stylua, -- lua
		-- formatting.shfmt, -- shell

		--[ diagnostics ]--
		diagnostics.dotenv_linter, -- shell
		diagnostics.flake8, -- python
		diagnostics.eslint_d.with({ -- js/ts linter
			-- only enable eslint if root has .eslintrc.js (not in youtube nvim video)
			condition = function(utils)
				return utils.root_has_file(".eslintrc.js") -- change file extension if you use something else
			end,
		}),

		--[ code_actions ]--
		code_actions.refactoring, -- python
		-- code_actions.shellcheck, -- shell

		--[ completion ]--
		-- completion.vsnip, -- all
		-- completion.luasnip, -- all

		--[ hover ]--
		hover.printenv,
		hover.dictionary,
	},
	-- configure format on save
	on_attach = function(current_client, bufnr)
		if current_client.supports_method("textDocument/formatting") then
			vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
			vim.api.nvim_create_autocmd("BufWritePre", {
				group = augroup,
				buffer = bufnr,
				callback = function()
					vim.lsp.buf.format({
						filter = function(client)
							return client.name == "null-ls" -- only use null-ls for formatting instead of lsp server
						end,
						bufnr = bufnr,
					})
				end,
			})
		end
	end,
})
