local status, db = pcall(require, "dashboard")
if not status then
  return
end


db.setup ({
  theme = 'hyper',
  config = {
    week_header = {
     enable = true,
    },
    shortcut = {
      { desc = ' Update', group = '@property', action = 'PackerUpdate', key = 'u' },
      {
        icon = ' ',
        icon_hl = '@variable',
        desc = 'Files',
        group = 'Label',
        action = 'Telescope find_files',
        key = 'f',
      },
    	{
        icon = ' ',
        icon_hl = '@variable',
    		desc = "Find HelpTags",
        group = 'Label',
    		action = "Telescope help_tags",
        key = 'h',
    	},
      {
        desc = ' KeyBindings',
        group = 'Number',
        action = 'Telescope keymaps',
        key = 'K',
      },
    },
    -- footer = { '', '', '', '🎉 Neovim is my choice of Editor!! --   ' },
  },
})

