local lsnip_status, ls = pcall(require, "luasnip")
if not lsnip_status then
	print("snippet not found")
	return
end

-- shorthands
local snip = ls.snippet
local node = ls.snippet_node
local text = ls.text_node
local insert = ls.insert_node
local func = ls.function_node
local choice = ls.choice_node
local dynamicn = ls.dynamic_node

local date = function()
	return { os.date("%Y-%m-%d") }
end

ls.add_snippets(nil, {
	all = {
		snip({
			trig = "cdate",
			namr = "Date",
			dscr = "Date in the form of YYYY-MM-DD",
		}, {
			func(date, {}),
		}),
	},

	sh = {

		snip({
			trig = "chead",
			namr = "header",
			dscr = "custom header for bash scripts",
		}, {
			text({ "#!/bin/env bash", "", "" }),
			text({ "# Author            - sum4n", "" }),
			text({ "# Source            - https://gitlab.com/sum4n", "" }),
			text({ "# Mail              - to_suman@outlook.com", "" }),
			text({ "# Created           - " }),
			func(date, {}),
			text({ "", "" }),
			text({ "# About             - " }),
			insert(1),
			text({ "", "", "", "" }),
			insert(0, "#code here.."),
		}),

		snip({
			trig = "celse",
			namr = "if-else",
			dscr = "if-else condition",
		}, {
			text({ "if [ " }),
			insert(1),
			text({ " ]; then", "  " }), -- "" used for new line
			insert(2),
			text({ "", "else", "  " }),
			insert(3),
			text({ "", "fi", "" }),
			insert(0),
		}),

		snip({
			trig = "celif",
			namr = "else-if",
			dscr = "if-elif condition",
		}, {
			text({ "if [ " }),
			insert(1),
			text({ " ]; then", "  " }), -- "" used for new line
			insert(2),
			text({ "", "elif [ " }),
			insert(3),
			text({ " ]; then", "  " }),
			insert(4),
			text({ "", "else", "  " }),
			insert(5),
			text({ "", "fi", "" }),
			insert(0),
		}),

		snip({
			trig = "ctest",
			namr = "test",
			dscr = "test condition",
		}, {
			text({ "" }),
			insert(1, "condition"),
			text({ " && " }),
			insert(2, "to do"),
		}),

		snip({
			trig = "cwhile",
			namr = "while",
			dscr = "while loop",
		}, {
			text({ "while " }),
			insert(1, "true"),
			text({ "; do", "  " }),
			insert(2),
			text({ "", "done", "" }),
			insert(0),
		}),
	},
})
