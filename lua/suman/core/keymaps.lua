--------------
-- leader-key
--------------
vim.g.mapleader = " "
local keymap = vim.keymap -- for conciseness

------------------
-- general keymaps
------------------

--
keymap.set("i", "ii", "<ESC>") -- uses ii for escape
keymap.set("n", "x", '"_x') -- used for x is not going to copy things
keymap.set("n", "<leader>S", ":saveas ") -- for save a file
keymap.set("n", "<leader>E", ":e ") -- for edit a new file
-- keymap.set("n", '<leader>jk', "<cmd>ToggleTerm size=45 direction=vertical<cr>")

-- only for vim surround
vim.cmd([[
  vmap s S
]])

-- resize with arrows
keymap.set("n", "<C-Right>", ":vertical resize +5<CR>") -- resizing vertical split
keymap.set("n", "<C-Left>", ":vertical resize -5<CR>") -- resizing vertical split
keymap.set("n", "<C-Up>", ":resize +5<CR>") -- resizing vertical split
keymap.set("n", "<C-Down>", ":resize -5<CR>") -- resizing vertical split

-- visual indent mode
keymap.set("v", "<", "<gv")
keymap.set("v", ">", ">gv")
keymap.set("v", "p", '"_dP')

----------------
-- which-key --
----------------

--
local status, which_key = pcall(require, "which-key")
if not status then
	return
end

--
local setup = {
	plugins = {
		marks = true, -- shows a list of your marks on ' and `
		registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
		spelling = {
			enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
			suggestions = 20, -- how many suggestions should be shown in the list?
		},
		presets = {
			operators = false, -- adds help for operators like d, y, ... and registers them for motion / text object completion
			motions = true, -- adds help for motions
			text_objects = true, -- help for text objects triggered after entering an operator
			windows = true, -- default bindings on <c-w>
			nav = true, -- misc bindings to work with windows
			z = true, -- bindings for folds, spelling and others prefixed with z
			g = true, -- bindings for prefixed with g
		},
	},
	icons = {
		breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
		separator = "➜", -- symbol used between a key and it's label
		group = "+", -- symbol prepended to a group
	},
	popup_mappings = {
		scroll_down = "<c-d>", -- binding to scroll down inside the popup
		scroll_up = "<c-u>", -- binding to scroll up inside the popup
	},
	window = {
		border = "double", -- none, single, double, shadow
		position = "bottom", -- bottom, top
		margin = { 1, 0, 1, 0 }, -- extra window margin [top, right, bottom, left]
		padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
		winblend = 0,
	},
	layout = {
		height = { min = 2, max = 25 }, -- min and max height of the columns
		width = { min = 20, max = 50 }, -- min and max width of the columns
		spacing = 3, -- spacing between columns
		align = "center", -- align columns left, center or right
	},
	ignore_missing = true, -- enable this to hide mappings for which you didn't specify a label
	hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "call", "lua", "^:", "^ " }, -- hide mapping boilerplate
	show_help = true, -- show help message on the command line when the popup is visible
	triggers = "auto", -- automatically setup triggers
	-- triggers = {"<leader>"} -- or specify a list manually

	triggers_blacklist = {
		i = { "j", "k" },
		v = { "j", "k" },
	},
}

local opts = {
	mode = "n", -- NORMAL mode
	prefix = "<leader>",
	buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
	silent = true, -- use `silent` when creating keymaps
	noremap = true, -- use `noremap` when creating keymaps
	nowait = true, -- use `nowait` when creating keymaps
}

local mappings = {
	["e"] = { "<cmd>NvimTreeToggle<cr>", "Explorer" },
	["w"] = { "<cmd>w!<CR>", "Save" },
	["q"] = { "<cmd>q!<CR>", "Quit" },
	["x"] = { "<cmd>wq!<CR>", "Write&Quit" },
	f = {
		name = "Telescope Finder",
		f = { "<cmd>Telescope find_files<cr>", "Find Files" },
		w = { "<cmd>Telescope live_grep<cr>", "Live_grep" },
		o = { "<cmd>Telescope oldfiles<cr>", "RecentFiles" },
		b = { "<cmd>Telescope buffers<cr>", "Buffers" },
		h = { "<cmd>Telescope help_tags<cr>", "HelpTags" },
		k = { "<cmd>Telescope keymaps<cr>", "Keymaps" },
		c = { "<cmd>Telescope colorscheme<cr>", "Colorscheme" },
		e = { "<cmd>Oil<cr>", "Open parent directory" },
	},
	p = {
		name = "Packer",
		c = { "<cmd>PackerCompile<cr>", "Compile" },
		i = { "<cmd>PackerInstall<cr>", "Install" },
		s = { "<cmd>PackerSync<cr>", "Sync" },
		S = { "<cmd>PackerStatus<cr>", "Status" },
		u = { "<cmd>PackerUpdate<cr>", "Update" },
	},
	g = {
		name = "Git",
		g = { "<cmd>lua _LAZYGIT_TOGGLE()<CR>", "Lazygit" },
		j = { "<cmd>lua require 'gitsigns'.next_hunk()<cr>", "Next Hunk" },
		k = { "<cmd>lua require 'gitsigns'.prev_hunk()<cr>", "Prev Hunk" },
		l = { "<cmd>lua require 'gitsigns'.blame_line()<cr>", "Blame" },
		p = { "<cmd>lua require 'gitsigns'.preview_hunk()<cr>", "Preview Hunk" },
		r = { "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", "Reset Hunk" },
		R = { "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", "Reset Buffer" },
		S = { "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", "Stage Hunk" },
		u = {
			"<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>",
			"Undo Stage Hunk",
		},
		s = { "<cmd>Telescope git_status<cr>", "Open changed file" },
		b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
		c = { "<cmd>Telescope git_commits<cr>", "Checkout commit" },
		d = {
			"<cmd>Gitsigns diffthis HEAD<cr>",
			"Diff",
		},
		f = {
			name = "git_bcommits",
			c = { "<cmd>Telescope git_bcommits<cr>", "git_bcommits" },
		},
	},
	l = {
		name = "LSP",
		c = {
			name = "Code Action",
			a = { "<cmd>Lspsaga code_action<CR>", "code_action" },
			d = { "<cmd>Lspsaga show_cursor_diagnostics<cr>", "cursor_diagnostics" },
		},
		r = {
			name = "Rename",
			n = { "<cmd>Lspsaga rename<cr>", "Rename" },
		},
		g = {
			name = "Go to",
			f = { "<cmd>Lspsaga lsp_finder<cr>", "defination and references" },
			d = { "<Cmd>lua vim.lsp.buf.declaration()<CR>", "see declaration" },
			D = { "<cmd>Lspsaga peek_definition<CR>", "defination and make change in window" },
			i = { "<cmd>lua vim.lsp.buf.implementation()<cr>", "implementation" },
			o = { "<cmd>Lspsaga outline<CR>", "outline" },
			j = {
				"<cmd>Lspsaga diagnostic_jump_next<CR>",
				"Next Diagnostic",
			},
			k = {
				"<cmd>Lspsaga diagnostic_jump_prev<cr>",
				"Prev Diagnostic",
			},
		},
		d = {
			"<cmd>Lspsaga show_line_diagnostics<cr>",
			"Document Diagnostics",
		},
		w = {
			"<cmd>Telescope diagnostics<cr>",
			"Workspace Diagnostics",
		},
		f = { "<cmd>lua vim.lsp.buf.format{async=true}<cr>", "Format" },
		i = { "<cmd>LspInfo<cr>", "Info" },
		K = {
			"<cmd>Lspsaga hover_doc<CR>",
			"documentation under cursor",
		},
		l = { "<cmd>lua vim.lsp.codelens.run()<cr>", "CodeLens Action" },
		q = { "<cmd>lua vim.lsp.diagnostic.set_loclist()<cr>", "Quickfix" },
		s = { "<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols" },
		S = {
			"<cmd>Telescope lsp_dynamic_workspace_symbols<cr>",
			"Workspace Symbols",
		},
	},
	s = {
		name = "Split window",
		v = { "<C-w>v", "Vertically" },
		h = { "<C-w>s", "Horaizontally" },
		x = { ":close<CR>", "Close split" },
	},
	t = {
		name = "Tab manage",
		o = { "<cmd>tabnew<cr>", "New tab" },
		n = { "<cmd>tabn<cr>", "Next tab" },
		p = { "<cmd>tabp<cr>", "Previous tab" },
		x = { "<cmd>tabclose<cr>", "Close tab" },
	},
	b = {
		name = "Buffer",
		n = { "<cmd>bnext<cr>", "Next Buffer" },
		p = { "<cmd>bprevious<cr>", "Previous Buffer" },
	},
}

which_key.setup(setup)
which_key.register(mappings, opts)
