local opt = vim.opt --for conciseness

-- line number
opt.relativenumber = true
opt.number = true

-- tabs & indention
opt.tabstop = 2
opt.shiftwidth = 2
opt.expandtab = true
opt.autoindent = true

-- line wrapping
opt.wrap = false

-- search settigs
opt.ignorecase = true
opt.smartcase = true

-- cursor line
opt.cursorline = true

-- appearance
opt.termguicolors = true
opt.background = "dark"
opt.signcolumn = "auto"
opt.scrolloff = 8
opt.sidescrolloff = 15

-- backspace
opt.backspace = "indent,eol,start" -- backspace work properly

-- clipboard
opt.clipboard:append("unnamedplus")

-- split windows
opt.splitright = true
opt.splitbelow = true

opt.iskeyword:append("-")

-- using ld vim commands to configure
vim.cmd ([[

" set guicursor = ""
set noswapfile
set colorcolumn=90
set undofile
set nohlsearch
set guifont=JetBrainsMono\ Nerd\ Font:h18

]])
