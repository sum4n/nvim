vim.cmd([[

  ""---- Everforest ----""
  "" for dark or light
  set background=dark
  " set background=light

  " Set contrast.
  " This configuration option should be placed before `colorscheme everforest`.
  " Available values: 'hard', 'medium'(default), 'soft'
  let g:everforest_background = 'hard'
  " For better performance
  let g:everforest_better_performance = 1

  ""---- Gruvbox ----""
  " Set contrast.
  " This configuration option should be placed before `colorscheme gruvbox-material`.
  " Available values: 'hard', 'medium'(default), 'soft'
  let g:gruvbox_material_background = 'medium'
  " For better performance
  let g:gruvbox_material_better_performance = 1

]])

local status, _ = pcall(vim.cmd, "colorscheme nightfox")
if not status then
	print("colorscheme not found!")
	return
end
