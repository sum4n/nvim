require("suman.plug-setup")
require("suman.core.options")
require("suman.core.keymaps")
require("suman.core.colorscheme")
require("suman.plugins.lualine")
-- require("suman.plugins.coc_conf")
require("suman.plugins.nvim_tree")
require("suman.plugins.telescope")
require("suman.plugins.colorizer")
require("suman.plugins.comment")
require("suman.plugins.nvim-cmp")
require("suman.snippet.my-snip")
require("suman.plugins.treesitter")
require("suman.plugins.gitsigns")
require("suman.plugins.dashboard")
require("suman.plugins.autopairs")
require("suman.plugins.which-key")
require("suman.plugins.lsp.mason")
require("suman.plugins.lsp.null-ls")
require("suman.plugins.lsp.lspconfig")
require("suman.plugins.lsp.lspsaga")
require("suman.plugins.toggleterm")
-- require("suman.plugins.blankline-indent")
require("suman.plugins.oil")
-- require("suman.plugins.impatient")
